package TestRegistration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertTrue;

public class TestRegistration {
    public  ChromeDriver driver;

    @Before
    public  void setUp(){
        System.setProperty("webdriver.crome.driver", "../TestRegistration/cromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://my.rozetka.com.ua/signup/");
    }

    @After
    public void tearDown(){
        driver.close();
    }

    @Test
    public void testRegistration1() throws InterruptedException {
        RegistrationPage Rpage = new RegistrationPage(driver); //пейдж обджект который отображает страницу регистрации
        ResultPage name = Rpage.setName("test");
        ResultPage email = Rpage.setEmail("test129721@test.test");//передаю емаил
        ResultPage password = Rpage.setPassword("Test123");
        ResultPage registerbutton = Rpage.pressRegBut();
        assertTrue(registerbutton.getTitleProfile().contains("Личные данные"));
    }

    @Test
    public void testRegistration2() throws InterruptedException {
        RegistrationPage Rpage = new RegistrationPage(driver); //пейдж обджект который отображает страницу регистрации
        ResultPage name = Rpage.setName("test");
        ResultPage email = Rpage.setEmail("test129421");//передаю некорректный емаил
        ResultPage password = Rpage.setPassword("Test123");
        ResultPage registerbutton = Rpage.pressRegBut();
        assertTrue(registerbutton.getTitleProfile().contains("Личные данные"));
    }
}
