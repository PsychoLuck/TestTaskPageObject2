package TestRegistration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class RegistrationPage {

    private WebDriver driver;//можно передать любой дравайвер

    public RegistrationPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public ResultPage setName(String string) {
      driver.findElement(By.xpath("(//INPUT[@type='text'])[2]")).sendKeys(string);
      return null;
    }

    public ResultPage setEmail(String s) {
        driver.findElement(By.xpath("(//INPUT[@type='text'])[3]")).sendKeys(s);
        return null;
    }

    public ResultPage setPassword(String test123) {
        driver.findElement(By.xpath("//INPUT[@type='password']")).sendKeys(test123);
        return null;
    }

    public ResultPage pressRegBut() throws InterruptedException {
        driver.findElement(By.xpath("//BUTTON[@class='btn-link-i'][text()='Зарегистрироваться']")).click();
        TimeUnit.SECONDS.sleep(3);
        return new ResultPage(driver);
    }
}
