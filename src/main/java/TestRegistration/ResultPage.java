package TestRegistration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ResultPage {

    private WebDriver driver;

    public ResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getTitleProfile() {
        return  driver.findElement(By.xpath("//H1[text()='Личные данные']")).getText();
    }
}
